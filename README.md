# Ask and Ye Shall Receive

## Challenge description
Ophelia FlagDeCotour just won a Hugo award! (Alongside hundreds of other people...) Go give her some congratulations

## Required files for hosting
None -- all is publicly accessible via the Internet.

## Hosting instructions
Sending the email for the flag no longer works, as I only had out-of-office replies with the flag setup for the duration of the CTF. 

As a general note, any account not obviously affiliated with TUCTF and this challenge should NOT be messaged, as they're real people. 

## Hints
- Do some OSINT on Ophelia!
- Look at the Hugo award winners for 2019 -- how might Ophelia have an account on one of them?
- If her name returns nothing, try making a simple username for her with what little you know.
- How might you contact Ophelia? (Once they have found the tumblr)
- If the webpage mentions ctf/TUCTF, you're on the right track!

## Flag 
`TUCTF{F0LL0W_7H3_D1G174L_CRUM8S}`